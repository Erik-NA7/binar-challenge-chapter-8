import Form from '../components/CreateForm';

const CreatePlayer = () => {

  return (
    <div className="container menu">
      <Form/>
    </div>
  );
}

export default CreatePlayer;