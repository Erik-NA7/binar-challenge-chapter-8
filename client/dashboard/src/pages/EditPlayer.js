import Form from '../components/EditForm';

const EditPlayer = () => {

  return (
    <div className="container menu">
      <Form/>
    </div>
  );
}

export default EditPlayer;