import Form from '../components/FindForm';

const FindPlayer = () => {
  return (
    <div className="container menu">
      <Form/>
    </div>
  );
}

export default FindPlayer;