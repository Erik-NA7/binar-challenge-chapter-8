import React from 'react';
import { Link } from 'react-router-dom';
import './pages.css'

const Home = () => {
  return (
    <div className="container menu">
      <Link to='/create'>
        <div className="menu-button">
          <button className="btn btn-primary button-links">Create Player</button>
        </div>
      </Link>
      <Link to='/find'>
        <div className="menu-button">
          <button className="btn btn-primary button-links">Find Player</button>
        </div>
      </Link>
      <Link to='/edit'>
        <div className="menu-button">
          <button className="btn btn-primary button-links">Edit Player</button>
        </div>
      </Link>
    </div>
  )
}

export default Home;