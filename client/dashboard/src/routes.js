import { Switch, Route, } from "react-router-dom";

import Create from './pages/CreatePlayer';
import Find from './pages/FindPlayer';
import Edit from './pages/EditPlayer';

export function Routes() {
  return (
    <Switch>
      <Route path="/create">
        <Create />
      </Route>
      <Route path="/find">
        <Find />
      </Route>
      <Route path="/edit">
        <Edit />
      </Route>
    </Switch>
  )
}