import { BrowserRouter as Router } from 'react-router-dom';
import { Routes } from './routes';
import Home from './pages/Home'
import './App.css'

function App() {
  return (
    <Router>
      <h3 className="text-center">Welcome to Dashboard</h3>
      <Home/>
      <Routes/>
    </Router>
  );
}

export default App;
