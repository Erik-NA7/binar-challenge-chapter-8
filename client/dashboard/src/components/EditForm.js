import React, { useState } from "react";
import './components.css'

const Form = ()=> {
  const [usernameInput, setUsername] = useState('user_example');
  const [emailInput, setEmail] = useState('example@email.com');
  const [passwordInput, setPassword] = useState('password_example');
  const [experienceInput, setExperience] = useState(1000);
  const [lvlInput, setLvl] = useState(1);
  const [title, setTitle] = useState('Edit Player')
  const [showForm, hideForm] = useState(false)
  const [hideData, showData] = useState(true)
  
  const getUsername = (data) => {
    setUsername(data.target.value)
  };

  const getEmail = (data) => {
    setEmail(data.target.value)
  }

  const getPassword = (data) => {
    setPassword(data.target.value)
  }

  const getExperience = (data) => {
    setExperience(data.target.value)
  }

  const getLvl = (data) => {
    setLvl(data.target.value)
  }

  const submitHandler = (e) => {
    e.preventDefault()
    hideForm(true);
    showData(false);
    setTitle('Player Edited');
  }

  const reset = () => {
    setUsername('');
    setEmail('');
    setPassword('');
    setExperience('');
    setLvl('');
  }
  
  const backHandler = (e) => {
    reset()
    hideForm(false)
    showData(true)
    setTitle('Edit Player')
  }

  return (
    <>
      <h5 className="text-center">{title}</h5>
      <form onSubmit={submitHandler} hidden={showForm}>
        <div className="form-group">
          <label id="username">Username</label><br/>
          <input value={usernameInput} onChange={getUsername} type="text" name="username" className="form-control" placeholder="Enter username"/>
          <label id="email">Email</label><br/>
          <input value={emailInput} onChange={getEmail} type="email" name="email" className="form-control" placeholder="Enter email"/>
          <label id="password">Password</label><br/>
          <input value={passwordInput} onChange={getPassword} type="password" name="password" className="form-control" placeholder="Enter password"/>
          <label id="experience">Experience</label><br/>
          <input value={experienceInput} onChange={getExperience} type="number" name="experience" className="form-control" placeholder="Enter experience"/>
          <label id="lvl">Lvl</label><br/>
          <input value={lvlInput} onChange={getLvl} type="number" name="Level" className="form-control" placeholder="Enter level"/>
          <button id="submit-button" type="submit" className="btn btn-primary">Submit</button>
        </div>
      </form>
      <div className="container" hidden={hideData}>
        <div className="container player-data">
          <div className="row">
            <div className="col data-field">Username</div>
            <div className="col data-value">{usernameInput}</div>
          </div>
          <div className="row">
            <div className="col data-field">Email</div>
            <p className="col data-value">{emailInput}</p>
          </div>
          <div className="row">
            <div className="col data-field">Password</div>
            <p type="password" className="col data-value">{passwordInput}</p>
          </div>
          <div className="row">
            <div className="col data-field">Experience</div>
            <p className="col data-value">{experienceInput}</p>
          </div>
          <div className="row">
            <div className="col data-field">Level</div>
            <p className="col data-value">{lvlInput}</p>
          </div>
        </div>
        <br/>
        <div className="container action-menu">
          <div className="back">
            <button onClick={backHandler} type="submit" className="btn btn-primary action-button">Back</button>
          </div>
          {/* <div className="edit">
            <button onClick={editHandler} type="submit" className="btn btn-primary action-button">Edit</button>
          </div> */}
        </div>
      </div>
      
    </>
  )
}

export default Form;