# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, after installing all dependencies with 
### 'npm i'
you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### Home Page

There are three menu buttons:
1. Create player
Klik the button to show form for entering player data. Click submit to show the data entered. Click back to go back to the form (the form is cleared)

2. Find player
Klik the button to show form for entering search criteria. Click submit to show the data entered. Click back to go back to the form (the form is cleared)

3. Edit player
Klik the button to show form for editing prefilled player data. Click submit to show the data entered/edited. Click back to go back to the form (the form is cleared).
